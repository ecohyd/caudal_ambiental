# Instrucciones

Requerimientos para usar el script

- Rstudio v1.2.5042
- R for windows v3.6.3
- Rtools v3.5

Uso del script, son tres pasos, 

1. formatear el archivo de datos historicos*
2. configurar rio y otros parámetros en main.R (desde linea 8)
3. apretar botón "source" en Rstudio

Ejemplo de configuración en main.R (desde linea 8)

```bash
nombre_csv <- "rio4.csv"
#Umbrales de metricas hidraulicas
q_banca_llena<-1328.27
q_t_q <-36.73
#caudales extremos interanuales
q_min<-25.51
q_max<-1834.79
```

*Ejemplo de formato archivo.csv

```bash
Fecha,QNat
01-01-2001,217.13
02-01-2001,178.76
03-01-2001,213.07
04-01-2001,238.44
05-01-2001,305.16
06-01-2001,319.66
07-01-2001,277.13
08-01-2001,236.66
09-01-2001,209.37
10-01-2001,189.82
...
```